<%@ page import="java.util.Date" %><%--
  Created by IntelliJ IDEA.
  User: kingk
  Date: 12/16/2020
  Time: 9:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Test JSP</title>
</head>
<body>

<h1>JSP Expressions</h1>
<p>JSP Expression tags allow you to directly put Java expressions into an HTML file. This includes math expressions, boolean expressions, and calling methods that return values to be displayed on the page.</p>
<p>Here are a few examples: </p>
<br>

<p>Calling a method: <%= Math.addExact(2, 3) %></p>

<p>Evaluating a boolean: <%= "bob".length() < 3 %></p>

<p>Math expression: <%= (500.6 * 7) - 50 %></p>

<p>Java object: <%= new Date() %></p>

<p>In each of these examples, they are implicitly being converted into Strings before being added to the HTML.</p>

</body>
</html>